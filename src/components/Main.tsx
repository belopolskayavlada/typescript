import * as React from "react";
import { Col, Container, Row } from "reactstrap";
import LeftSide from "./LeftSide";
import RightSide from "./RightSide";

export interface IObjOne {
  id: number;
  name: string;
  surname: string;
}

interface IObjTwo {
  age: number;
  gender: string;
}

export interface IData {
  tableOne: IObjOne[];
  tableTwo: IObjTwo[];
}

interface IState {
  data: IData;
}

class Main extends React.Component<any, IState> {
  constructor(props: any) {
    super(props);
    this.state = {
      data: {
        tableOne: [
          { id: 1, name: "name1", surname: "surname1" },
          { id: 2, name: "name2", surname: "surname2" },
          { id: 3, name: "name3", surname: "surname3" },
          { id: 4, name: "name4", surname: "surname4" }
        ],
        tableTwo: [
          { age: 45, gender: "female" },
          { age: 35, gender: "male" },
          { age: 35, gender: "male" },
          { age: 55, gender: "female" },
          { age: 65, gender: "female" },
          { age: 75, gender: "male" }
        ]
      }
    };
  }

  public render() {
    return (
      <Container>
        <Row>
          <Col>
            <h1>React TypeScript Table</h1>
          </Col>
        </Row>
        <Row>
          <Col sm="4">
            <LeftSide />
          </Col>
          <Col sm="auto">
            <RightSide data={this.state.data} />
          </Col>
        </Row>
      </Container>
    );
  }
}

export default Main;
