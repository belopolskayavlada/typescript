import * as React from "react";
import CustomTable from "./CustomTable/CustomTable";
import { IData } from "./Main";

interface IProps {
  data: IData;
}

class RightSide extends React.Component<IProps> {
  public render() {
    return <CustomTable data={this.props.data} />;
  }
}

export default RightSide;
