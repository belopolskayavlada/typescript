import * as React from "react";
import { IObjOne } from "../Main";

interface IProps {
  tag: string;
  inner: IObjOne;
}

const TableRow = (props: IProps): any => {
  const newArr = Object.keys(props.inner);

  return (
    <tr>
      {props.tag === "th"
        ? newArr.map(item => {
            return <th key={item}>{item}</th>;
          })
        : newArr.map(item => {
            return <td key={item}>{props.inner[item]}</td>;
          })}
    </tr>
  );
};

export default TableRow;
