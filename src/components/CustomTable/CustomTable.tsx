import * as React from "react";
import { Table } from "reactstrap";
import { IData } from "../Main";

import TableRow from "./TableRow";

interface IProps {
  data: IData;
}

class CustomTable extends React.Component<IProps> {
  public render() {
    return (
      <Table>
        <thead>
          <TableRow tag="th" inner={this.props.data.tableOne[0]} />
        </thead>
        <tbody>
          {this.props.data.tableOne.map(item => {
            return <TableRow tag="td" inner={item} key={item.id} />;
          })}
        </tbody>
      </Table>
    );
  }
}

export default CustomTable;
