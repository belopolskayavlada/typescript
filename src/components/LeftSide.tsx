import * as React from "react";
import { Button, Col, Container, Row } from "reactstrap";

class LeftSide extends React.Component {
  public render() {
    return (
      <Container>
        <Row>
          <Col>
            <Button color="primary">Первая таблица</Button>
          </Col>
        </Row>
        <Row>
          <Col>
            <Button color="primary">Вторая таблица</Button>
          </Col>
        </Row>
      </Container>
    );
  }
}

export default LeftSide;
